package com.khrystyna;

import com.khrystyna.views.ConsoleView;
import com.khrystyna.views.View;

public class Application {
    public static void main(String[] args) {
        View view = new ConsoleView();

        view.run();
    }
}
