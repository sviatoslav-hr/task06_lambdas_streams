package com.khrystyna.views;

import com.khrystyna.controllers.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class ConsoleView implements View {
    private Controller controller = new Controller();
    private static Logger logger = LogManager.getLogger(ConsoleView.class);
    private Scanner scanner = new Scanner(System.in);

    @Override
    public void run() {
        startTask1();
        startTask2();
        startTask3();
        startTask4();
    }

    private void startTask1() {
        logger.trace("Task 1.\n" +
                controller.getTask1Results());
    }

    private void startTask2() {
        String input;
        logger.trace("\nTask 2.");
        controller.initTask2();
        do {
            logger.trace("Type operation name [add, subtract, multiply, divide, exit]: ");
            input = scanner.next();
            if (input.equals("exit")) {
                break;
            } else {
                logger.trace(controller.getTask2Result(input));
            }
        } while (!input.trim().isEmpty());
    }

    private void startTask3() {
        logger.trace("\nTask 3.\n" + controller.getTask3Results());
    }

    private void startTask4() {
        String input;
        logger.trace("\nTask 4.\nType your text");
        scanner.nextLine();
        do {
            input = scanner.nextLine();
            controller.saveLine(input);
        } while (input.trim().length() > 0);

        logger.trace(controller.getTask4Results());
    }
}
