package com.khrystyna.model;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StringService {
    private List<String> list = new LinkedList<>();

    public void save(String s) {
        list.add(s);
    }

    public long getUniqueWordsNumber() {
        return list.stream()
                .flatMap(e -> Stream.of(e.split(" ")))
                .map(String::toLowerCase)
                .collect(Collectors.groupingBy(w -> w, Collectors.counting()))
                .entrySet().stream()
                .filter(e -> e.getValue() == 1)
                .count();
    }

    public List<String> getSortedUniqueWords() {
        return list.stream()
                // split all words to one list
                .flatMap(e -> Stream.of(e.split(" ")))
                .map(String::toLowerCase)
                // Build a map from word -> frequency
                .collect(Collectors.groupingBy(w -> w, Collectors.counting()))
                // stream the frequency map entries
                .entrySet().stream()
                // filter to retain unique words (with frequency == 1)
                .filter(e -> e.getValue() == 1)
                // get only keys from map
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }

    public Map<String, Long> countWordsOccurrences() {
        return list.stream()
                .flatMap(e -> Stream.of(e.split(" ")))
                .collect(Collectors.groupingBy(w -> w, Collectors.counting()));
    }

    public Map<String, Long> countLowercaseCharOccurrences() {
        return list.stream()
                .flatMap(e -> Stream.of(e.split("")))
                .filter(s -> s.toLowerCase().equals(s) && !s.trim().isEmpty())
                .collect(Collectors.groupingBy(w -> w, Collectors.counting()));
    }
}
