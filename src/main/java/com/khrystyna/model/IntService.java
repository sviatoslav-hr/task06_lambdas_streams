package com.khrystyna.model;

import com.khrystyna.model.command.IntOperation;

import java.util.*;

public class IntService {
    private static final int ARRAY_MIN = 1;
    private static final int ARRAY_MAX = 10;
    private static final int LIST_SIZE = 20;
    private List<Integer> list = new LinkedList<>();

    public IntService() {
        Random random = new Random();
        for (int i = 0; i < LIST_SIZE; i++) {
            list.add(ARRAY_MIN + random.nextInt(ARRAY_MAX - ARRAY_MIN));
        }
    }

    public int calculate(IntThreeParamOperator operator) {
        return operator.calculate(list.get(0), list.get(1), list.get(2));
    }

    public int calculate(IntOperation operation) {
        return operation.calculate(list.get(0), list.get(1));
    }

    public double getAverage() {
        OptionalDouble average = list.stream()
                .mapToInt(Integer::intValue)
                .average();
        if (average.isPresent()) {
            return average.getAsDouble();
        } else {
            return -1;
        }
    }

    public int getMax() {
        OptionalInt min = list.stream()
                .mapToInt(Integer::intValue)
                .max();
        if (min.isPresent()) {
            return min.getAsInt();
        } else {
            return -1;
        }
    }

    public int getMin() {
        OptionalInt max = list.stream()
                .mapToInt(Integer::intValue)
                .min();
        if (max.isPresent()) {
            return max.getAsInt();
        } else {
            return -1;
        }
    }

    public int getSum() {
        return list.stream()
                .mapToInt(Integer::intValue)
                .sum();
    }

    public int subtract(int a, int b) {
        return a - b;
    }

    public List<Integer> getList() {
        return list;
    }

    public String getSublistTo(int toIndex) {
        if (toIndex <= 0) {
            toIndex = 1;
        } else if (toIndex > list.size() - 1) {
            toIndex = list.size() - 1;
        }
        return list.subList(0, toIndex).toString();
    }

    @Override
    public String toString() {
        return list.toString();
    }
}
