package com.khrystyna.model;

@FunctionalInterface
public interface IntThreeParamOperator {
    int calculate(int a, int b, int c);
}
