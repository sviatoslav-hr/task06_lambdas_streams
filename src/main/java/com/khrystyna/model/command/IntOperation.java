package com.khrystyna.model.command;

@FunctionalInterface
public interface IntOperation {
    int calculate(int a, int b);
}
