package com.khrystyna.model.command;

public class IntAddOperation implements IntOperation {
    @Override
    public int calculate(int a, int b) {
        return a + b;
    }
}
