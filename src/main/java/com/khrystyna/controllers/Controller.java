package com.khrystyna.controllers;

import com.khrystyna.model.IntThreeParamOperator;
import com.khrystyna.model.IntService;
import com.khrystyna.model.StringService;
import com.khrystyna.model.command.IntAddOperation;
import com.khrystyna.model.command.IntOperation;

import java.util.HashMap;
import java.util.Map;

public class Controller {
    private StringService stringService = new StringService();
    private IntService intService = new IntService();
    private Map<String, IntOperation> intOperations;

    public String getTask1Results() {
        StringBuilder result = new StringBuilder();
        result.append("Generated array: ")
                .append(intService.getSublistTo(3));

        IntThreeParamOperator maxOperator = (a, b, c) -> Math.max(Math.max(a, b), c);
        IntThreeParamOperator averageOperator = (a, b, c) -> (a + b + c) / 3;

        result.append("\nMax = ").append(intService.calculate(maxOperator))
                .append("\nAverage = ").append(intService.calculate(averageOperator));
        return result.toString();
    }

    public void initTask2() {
        intOperations = new HashMap<>();
        intOperations.put("add", new IntAddOperation());
        intOperations.put("subtract", intService::subtract);
        intOperations.put("multiply", (a, b) -> a * b);
        intOperations.put("divide", new IntOperation() {
            @Override
            public int calculate(int a, int b) {
                return a / b;
            }
        });
    }

    public String getTask2Result(String operationName) {
        Integer a = intService.getList().get(0);
        Integer b = intService.getList().get(1);
        int calculated = intOperations.get(operationName.toLowerCase())
                .calculate(a, b);
        return operationName + "(" + a + ", " + b + ") = " + calculated;
    }

    public String getTask3Results() {
        return "List: " + intService.toString()
                + "\nAverage = " + intService.getAverage()
                + "\nMin = " + intService.getMin()
                + "\nMax = " + intService.getMax()
                + "\nSum = " + intService.getSum();
    }

    public void saveLine(String s) {
        if (s.trim().length() > 0) {
            stringService.save(s);
        }
    }

    public String getTask4Results() {
        return "Sorted unique words (" + stringService.getUniqueWordsNumber()
                + "): " + stringService.getSortedUniqueWords().toString()
                + "\nWords occurrences: "
                + stringService.countWordsOccurrences().toString()
                + "\nLowercase chars occurrences: "
                + stringService.countLowercaseCharOccurrences().toString();
    }

    @Override
    public String toString() {
        return "Controller{" +
                "stringService=" + stringService +
                '}';
    }
}
